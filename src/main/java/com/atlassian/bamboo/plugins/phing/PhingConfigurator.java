package com.atlassian.bamboo.plugins.phing;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.build.Job;

import com.google.common.collect.Sets;
import com.opensymphony.xwork2.TextProvider;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;

public class PhingConfigurator extends AbstractTaskConfigurator implements BuildTaskRequirementSupport
{
    protected static final Set<String> FIELDS_TO_COPY = Sets.newHashSet();

    public static final String RUNTIME = "runtime";
    public static final String TARGET = "target";
    public static final String CONFIG = "config";
    public static final String ARGUMENTS = "arguments";

    public static final String CTX_UI_CONFIG_BEAN = "uiConfigSupport";
    private static final String DEFAULT_TEST_RESULTS_PATTERN =  "**/test-reports/*.xml";

    public TextProvider textProvider;
    public UIConfigSupport uiConfigSupport;

    static {
        FIELDS_TO_COPY.add(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY);
        FIELDS_TO_COPY.add(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN);
        FIELDS_TO_COPY.add(TaskConfigConstants.CFG_HAS_TESTS_BOOLEAN);
        FIELDS_TO_COPY.add(TaskConfigConstants.CFG_HAS_TESTS);
        FIELDS_TO_COPY.add(RUNTIME);
        FIELDS_TO_COPY.add(TARGET);
        FIELDS_TO_COPY.add(CONFIG);
        FIELDS_TO_COPY.add(ARGUMENTS);
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition, @NotNull Job job)
    {
        final String runtime = taskDefinition.getConfiguration().get(RUNTIME);
        Preconditions.checkNotNull(runtime, "No Phing executable was selected");

        return Sets.<Requirement>newHashSet(new RequirementImpl(
            PhingTaskType.CAPABILITY_PREFIX + "." + runtime, true, ".*"
        ));
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        if (StringUtils.isEmpty(params.getString(PhingConfigurator.RUNTIME)))
        {
            errorCollection.addError(PhingConfigurator.RUNTIME, textProvider.getText("phing.runtime.error"));
        }

        if (StringUtils.isEmpty(params.getString(PhingConfigurator.TARGET)))
        {
            errorCollection.addError(PhingConfigurator.TARGET, textProvider.getText("phing.target.error"));
        }
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(
            config, params, FIELDS_TO_COPY
        );

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        populateContextForAllOperations(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAllOperations(context);

        taskConfiguratorHelper.populateContextWithConfiguration(
            context, taskDefinition, FIELDS_TO_COPY
        );
    }

    public void populateContextForAllOperations(@NotNull Map<String, Object> context)
    {
        context.put(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN, DEFAULT_TEST_RESULTS_PATTERN);
        context.put(TaskConfigConstants.CFG_HAS_TESTS_BOOLEAN, Boolean.FALSE);
        context.put(TaskConfigConstants.CFG_HAS_TESTS, Boolean.FALSE);
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
    }

    public void setTextProvider(TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }

    public void setUiConfigSupport(UIConfigSupport uiConfigSupport)
    {
        this.uiConfigSupport = uiConfigSupport;
    }
}
